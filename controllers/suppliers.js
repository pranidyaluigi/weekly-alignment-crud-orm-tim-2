const { supplier } = require("../models");

class Supplier {
  async getAllSupplier(req, res, next) {
    try {
      let data = await supplier.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Suppliers were not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async getDetailSupplier(req, res, next) {
    try {
      let data = await supplier.findOne({ where: { id: req.params.id } });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["supplier was not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: "Internal Server Error" });
    }
  }
  async createSupplier(req, res, next) {
    try {
      let data = await supplier.create(req.body);
      //   const data = await supplier.findOne({ where: { id: req.params.id } });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["ID supplier was not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500), json({ errors: ["Internal Server Error"] });
    }
  }
  async updateSupplier(req, res, next) {
    try {
      const updatedData = await supplier.update(req.body, {
        where: { id: req.params.id },
      });

      if (updatedData === 0) {
        return res.status(404).json({ errors: ["ID Supplier was not found"] });
      }

      const data = await supplier.findOne({ where: { id: req.params.id } });

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async deleteSupplier(req, res, next) {
    try {
      let data = await supplier.destroy({ where: { id: req.params.id } });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["ID supplier was not found"] });
      }

      res.status(200).json({ message: "Success delete supplier" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Supplier();
