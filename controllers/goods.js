const { good, supplier } = require("../models");

class Good {
  async getAllGoods(req, res, next) {
    try {
      const data = await good.findAll({
        where: {},
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: supplier,
            attributes: ["name"],
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async getDetailGood(req, res, next) {
    try {
      const data = await good.findOne({
        where: {
          id: req.params.id,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: supplier,
            attributes: ["name"],
          },
        ],
      });

      if (data === null) {
        return res.status(404).json({ errors: ["Good not found"] });
      }

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async createGood(req, res, next) {
    try {
      const newData = await good.create(req.body);

      const data = await good.findOne({
        where: {
          id: newData.id,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: supplier,
            attributes: ["name"],
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async updateGood(req, res, next) {
    try {
      const updatedData = await good.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      if (updatedData[0] === 0) {
        return res.status(404).json({ errors: ["Goods not found"] });
      }

      const data = await good.findOne({
        where: {
          id: req.params.id,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: supplier,
            attributes: ["name"],
          },
        ],
      });

      if (data === null) {
        return res.status(404).json({ errors: ["Good not found"] });
      }

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteGood(req, res, next) {
    try {
      const deletedData = await good.destroy({
        where: {
          id: req.params.id,
        },
      });

      if (!deletedData) {
        return res
          .status(404)
          .json({ errors: ["Good not found or Has been deleted"] });
      }

      res.status(201).json({ message: ["Success Deleting Data"] });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Good();
